sed -r "/[ '’-]/d;/^.{1,4}$/d;/\(/d" wordlist.brut | unaccent UTF-8 | sort | uniq > wordlist.real

while read word
do
  if grep -q -r ${word}e wordlist.real
  then
    echo ${word}e >> wordlist.remove
  fi
  if grep -q -r ${word}s wordlist.real
  then echo ${word}s >> wordlist.remove
  fi
done < wordlist.real

rm -f wordlist
while read word
do
  if ! grep -q -r "${word}" wordlist.remove
  then
    echo ${word} >> wordlist
  fi
done < wordlist.real
