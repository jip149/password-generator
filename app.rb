require 'securerandom'

require 'bundler'
Bundler.require

require 'sinatra/asset_pipeline'

class App < Sinatra::Base
  set :relative_url_root, ''
  register Sinatra::AssetPipeline
  self.sprockets.css_compressor = :yui
  self.sprockets.js_compressor = :uglifier

  set :markdown, :layout_engine => :haml

  get '/' do
    @seed = SecureRandom.hex
    haml :index, :locals => {:assets_prefix => settings.relative_url_root + '/' + settings.assets_prefix}
  end

end
