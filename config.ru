require './app'

log = File.new("log/sinatra.log", "a")
$stdout.reopen(log)
$stderr.reopen(log)

relative_url_root = ENV['RACK_RELATIVE_URL_ROOT'] || '/'

map relative_url_root do
  App.set :relative_url_root, relative_url_root
  run App
end
